Certainly! The BBC has a rich archive of cerebral and cultural radio programs that span decades. Here are some suggestions similar to "In Our Time" that you might find intriguing:

1. **The Reith Lectures**:
   - Named after the BBC's first director-general, John Reith, this series features leading thinkers delivering lectures on various topics. Over the years, it has included contributions from Bertrand Russell, Robert Oppenheimer, and more recently Stephen Hawking and Grayson Perry.

2. **Desert Island Discs**:
   - While primarily known for its focus on music choices of notable individuals, the conversations often delve deeply into personal philosophies and cultural reflections.

3. **A History of the World in 100 Objects**:
   - Presented by Neil MacGregor, then Director of the British Museum, this series explores world history through objects from the museum's collection.

4. **Thinking Allowed**:
   - Hosted by Laurie Taylor, this program discusses new research in sociology and related fields with academics and experts.

5. **The Essay**:
   - A series where writers reflect on arts, culture, history, philosophy, religion or science in a short essay format.

6. **Great Lives**:
   - In each episode, a guest nominates someone who has inspired them or whom they admire greatly; together with an expert biographer or historian they discuss their life and legacy.

7. **Night Waves (now Free Thinking)**:
   - This program covers ideas in contemporary culture including literature, film, music and art as well as intellectual debates about society at large.

8. **Front Row**:
   - A daily magazine program covering all aspects of the arts including interviews with leading figures from literature to theatre to visual arts.

9. **Beyond Belief**:
   - A discussion program exploring how faith influences our values and perspectives on current issues.

10. **Start the Week**: 
    - Hosted by Andrew Marr (and previously Melvyn Bragg), this show sets up discussions around books and ideas shaping our world today with authors and thinkers from various disciplines.
    
11. **Documentary Archive Programs like "Witness" or "The Documentary"**
    These programs offer deep dives into historical events through firsthand accounts or detailed explorations by historians.
    
12. **Analysis**
    This long-running series examines social sciences topics in depth with expert analysis on political economy to societal trends.
    
These programs should provide a wealth of intellectually stimulating content across various subjects over many years! You can often find these archived online via BBC Sounds or other archival services provided by the BBC.


# Mon  8 Jul 02:07:52 CEST 2024 - suggestion some cerebral and cultural BBC radio programs from the archives similar to In Our Time, going back over the last five decades